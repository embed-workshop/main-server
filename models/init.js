var options = {
}

var pgp = require('pg-promise')(options)
var cn = {
    host:     'localhost', 
    port:      5432,
    database: 'pi',
    user:     'pi',
    password: 'raspberry'
}

var db = pgp(cn)

// tx_1_start
db.tx(t => {
    return t.batch([

        t.query(
`
CREATE TABLE IF NOT EXISTS users (
    id                     SERIAL PRIMARY KEY,
    login                  VARCHAR(1024) NOT NULL,
    last_name              VARCHAR(1024) DEFAULT '',
    first_name             VARCHAR(1024) DEFAULT '',
    password               VARCHAR(1024) NOT NULL,
    status                 INT DEFAULT 1
)
`
        ),
        t.query(
`
CREATE TABLE IF NOT EXISTS station_groups (
    id                     SERIAL PRIMARY KEY,
    title                  VARCHAR(1024) DEFAULT '',
    ord                    INT DEFAULT 0,
    status                 INT DEFAULT 1
)
`
        ),
        t.query(
`
CREATE TABLE IF NOT EXISTS event_types (
    id                     SERIAL PRIMARY KEY,
    name                   VARCHAR(1024)               DEFAULT '',
    title                  VARCHAR(1024)               DEFAULT ''
)
`
        ),
    ])
})
.then(() => {

// tx_2_start
db.tx(t => {
    return t.batch([

        t.query(
`
INSERT INTO event_types (name, title) VALUES
    ('station_error',        'Ошибка устройства \"Логика станции\"'),
    ('weights_error',        'Ошибка устройства \"Показания весов\"'),
    ('entr_door_man_error',  'Ошибка устройства \"Управление входной дверью\"'),
    ('life_door_man_error',  'Ошибка устройства \"Управление дверью в жизнь\"'),
    ('death_door_man_error', 'Ошибка устройства \"Управление дверью на убой\"'),
    ('stimulation_error',    'Ошибка устройства \"Управление стимуляцией\"'),
    ('entr_door_st_error',   'Ошибка устройства \"Состояние входной двери\"'),
    ('life_door_st_error',   'Ошибка устройства \"Состояние двери в жизнь\"'),
    ('death_door_st_error',  'Ошибка устройства \"Состояние двери на убой\"'),
    ('weighing',             'Взвешивание'),
    ('death',                'Смерть свиньи'),
    ('correction',           'Коррекция количества свиней'),
    ('success_weighing',     'Отбор')
`
        ),
        t.query(
`
CREATE TABLE IF NOT EXISTS stations (
    id                     SERIAL PRIMARY KEY,
    cpuid                  VARCHAR(1024) UNIQUE NOT NULL,
    ip                     VARCHAR(1024) DEFAULT '', 
    title                  VARCHAR(1024) DEFAULT '',
    ord                    INT DEFAULT 0,
    status                 INT DEFAULT 1,
    state                  INT DEFAULT 0,
    station_group_id       INT REFERENCES station_groups(id),
    datetime               TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()
)
`
        ),
    ])
})
.then(() => {

// tx_3_start
db.tx(t => {
    return t.batch([

        t.query(
`
CREATE TABLE IF NOT EXISTS events (
    id                     SERIAL PRIMARY KEY,
    type_id                INT REFERENCES event_types(id),
    value_int              INT,
    value_text             TEXT,
    datetime               TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW() NOT NULL,
    status                 INT DEFAULT 1,
    station_id             INT REFERENCES stations(id),
    local_id               INT
)
`
        ),
        t.query(
`
CREATE TABLE batches (
    id                     SERIAL PRIMARY KEY,
    title                  VARCHAR(1024)               DEFAULT '',
    start_time             TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW() NOT NULL,
    end_time               TIMESTAMP WITHOUT TIME ZONE,
    start_count            INT,
    current_count          INT,
    status                 INT DEFAULT 1,
    station_id             INT REFERENCES stations(id),
    local_id               INT
)
`
        ),
    ])
})
.then(() => {

// tx_4_start
db.tx(t => {
    return t.batch([

        t.query(
`
CREATE INDEX type_id_idx ON events (type_id)
`
        ),
        t.query(
`
CREATE INDEX datetime_idx ON events (datetime)
`
        ),
        t.query(
`
CREATE INDEX status_idx ON events (status)
`
        ),
        t.query(
`
CREATE INDEX station_id_idx ON events (station_id)
`
        ),
    ])
})
.then(() => {

    console.log('done, press Ctrl-C')

})
.catch(error => {

    console.log(error)

})
// tx_4_end

})
.catch(error => {

    console.log(error)

})
// tx_3_end

})
.catch(error => {

    console.log(error)

})
// tx_2_end

})
.catch(error => {

    console.log(error)

})
// tx_1_end
