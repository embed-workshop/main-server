var options = {
}

var pgp = require('pg-promise')(options)
var cn = {
    host:     'localhost', 
    port:      5432,
    database: 'pi',
    user:     'pi',
    password: 'raspberry'
}

var db = pgp(cn)

// tx_1_start
db.tx(t => {
    return t.batch([

        t.query(
`
DROP TABLE IF EXISTS batches;
`
        ),
        t.query(
`
DROP TABLE IF EXISTS events;
`
        ),
    ])
})
.then(() => {

// tx_2_start
db.tx(t => {
    return t.batch([

        t.query(
`
DROP TABLE IF EXISTS stations;
`
        ),       
    ])
})
.then(() => {

// tx_3_start
db.tx(t => {
    return t.batch([

        t.query(
`
DROP TABLE IF EXISTS event_types;
`
        ),
        t.query(
`
DROP TABLE IF EXISTS station_groups;
`
        ),
        t.query(
`
DROP TABLE IF EXISTS users;
`
        ),
    ])
})
.then(() => {

    console.log('done, press Ctrl-C')

})
.catch(error => {

    console.log(error)

})
// tx_3_end

})
.catch(error => {

    console.log(error)

})
// tx_2_end

})
.catch(error => {

    console.log(error)

})
// tx_1_end
