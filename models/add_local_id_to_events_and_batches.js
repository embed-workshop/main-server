var options = {
}

var pgp = require('pg-promise')(options)
var cn = {
    host:     'localhost',
    port:      5432,
    database: 'pi',
    user:     'pi',
    password: 'raspberry'
}

var db = pgp(cn)

db.tx(t => {
    return t.batch([

        t.query(
`
ALTER TABLE batches ADD COLUMN local_id INT
`
        ),
        t.query(
`
ALTER TABLE events ADD COLUMN local_id INT
`
        ),
    ])
})
.then(() => {

    console.log('done, press Ctrl-C')

})
.catch(error => {

    console.log(error)

})

