const QueryFile = require('pg-promise').QueryFile;
const path      = require('path');

function sql(file) {
    const fullPath = path.join(__dirname, file);
    return new QueryFile(fullPath, {minify: true});
}

module.exports = {

    stations: {
        state:         sql('sql/stations/state.sql'),
        rename:        sql('sql/stations/rename.sql'),
        add:           sql('sql/stations/add.sql'),
        select:        sql('sql/stations/select.sql'),
        move_to_group: sql('sql/stations/move_to_group.sql'),
        add_group:     sql('sql/stations/add_group.sql'),
        update:        sql('sql/stations/update.sql'),
    },

    sync: {
        batches_max_start_time: sql('sql/sync/batches_max_start_time.sql'),
        events_max_datetime:    sql('sql/sync/events_max_datetime.sql'),
    },

};