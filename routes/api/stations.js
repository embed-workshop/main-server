var express = require('express')
var router  = express.Router()

const http  = require('http')

var log     = require('../../log.js')

// список всех станций из базы
router.get('/', function(req, res, next) {

    req.db.tx(t => {

        return t.batch([
            t.any( 'SELECT * FROM stations' ),
            t.any( 'SELECT * FROM station_groups' ),
        ])

    })
    .then(data => {
        var stations       = data[0],
            station_groups = data[1],
            // станции, у которых нет группы
            station_groups_by_id = {
                0: {
                    id:       0,
                    name:     'Новые станции',
                    ord:      0,
                    status:   1,
                    children: [],
                }
            }

        station_groups.forEach(station_group => {
            station_groups_by_id[station_group.id] = {
                id:       station_group.id,
                name:     station_group.title,
                ord:      station_group.ord,
                status:   station_group.status,
                children: [],
            }
        })

        stations.forEach(station => {
            var station_group = station_groups_by_id[ station.station_group_id || 0 ]
            if ( station_group ) {
                station_group.children.push({
                    id:    station.id,
                    cpuid: station.cpuid,
                    ip:    station.ip,
                    name:  station.title,
                    state: station.state,
                })
            }
        })

        res.json(Object.values( station_groups_by_id ))

    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })

})

// состояние станции:
// проверка даты последнего пинга от станции
router.get('/state', function(req, res, next) {

    req.db.manyOrNone( req.sql.stations.state )
    .then(data => {

        var result = {}
        data.forEach(datum => {
            result[datum.id] = datum.state
        })

        res.json({ success: 1, result: result })
    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })

})

// переименование станции
router.post('/:id/rename', function(req, res, next) {

    req.db.one(

        req.sql.stations.rename,
        { title: req.body.title, id: req.params.id } )

    .then(data => {

        res.json({ success: 1, data: data })

        // отправка запроса на переименование на станцию
        var url = `http://${data.ip}:3000/api/station_state/${data.title}`
        http.get( url, response => {

            if (response.statusCode !== 200) {
                log.error(`get request ${url} failed`);
                return;
            }

            var rawData = '';
            response.on('data', chunk => { rawData += chunk; });
            response.on('end', () => {
                try {
                    const parsedData = JSON.parse(rawData);
                    if (parsedData.success) {
                        log.info(`Station '${parsedData.old_title}' renamed to '${parsedData.new_title}' `)
                    }
                } catch (e) {
                    log.error(`get request ${url} processing failed: ${e.message}`);
                }
            });

        }).on('error', (e) => {
            log.error(`get request ${url} failed: ${e.message}`);
        });

    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })
})


router.post('/add', function(req, res, next) {
    req.db.one(
        req.sql.stations.add_group,
        { title: req.body.title }
    )
    .then(data => {
        log.info(`Created station group '${data.title}'`)
        res.json({ success: 1, title: data.title })
    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })
})

router.post('/:station_id/move_to_group/:station_group_id', function(req, res, next) {
    var station_id = req.params.station_id
    var station_group_id = req.params.station_group_id

    if (station_group_id === '0') {
        station_group_id = undefined
    }

    req.db.none(
        req.sql.stations.move_to_group,
        { station_group_id: station_group_id, id: station_id }
    )
    .then(data => {
        log.info(`Station '${station_id}' moved to station group '${station_group_id}'`)
        res.json({ success: 1 })
    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })
})

router.get('/connect', function(req, res, next) {

    var cpuid = req.query.cpuid
    var state = req.query.state

    var ip    = ''
    var adr = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var _r = adr.match(/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/g)
    if (_r && _r.length) {
       ip = _r[0] || '127.0.0.1'
    }

    req.db.oneOrNone(

        req.sql.stations.select,
        { cpuid: cpuid }

    )
    .then(data => {
        if (data && data.id) {
            var station_id = data.id
            req.db.none(
                req.sql.stations.update,
                { id: station_id, state: state }
            )
            .then(data => {
                res.json({ connected: 1})
            })
            .catch(error => {
                log.error(`route ${req.originalUrl}, db query error: ${error}`)
            })
            return
        }
        else {
            req.db.one(
                req.sql.stations.add,
                { cpuid: cpuid, ip: ip, title: 'Default-'+cpuid, state: state }
            )
            .then(data => {
                res.json({ success: 1, station_group: '', station_name: data.title })
            })
            .catch(error => {
                log.error(`route ${req.originalUrl}, db query error: ${error}`)
            })
        }
    })
    .catch(function(error) {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })
})

module.exports = router