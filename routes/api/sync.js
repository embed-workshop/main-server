const express = require('express')
const router  = express.Router()
var log       = require('../../log.js')
const pgp     = require('pg-promise')();

const http    = require('http')

router.get('/', function(req, res, next) {

    var cpuid = req.query.cpuid
    var db = req.db

    var ip    = ''
    var adr = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var _r = adr.match(/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/g)
    if (_r && _r.length) {
       ip = _r[0] || '127.0.0.1'
    }

    req.db.oneOrNone(
        req.sql.stations.select,
        { cpuid: cpuid }
    )
    .then(data => {
        if ( data && data.id ) {
            var station_id  = data.id
            req.db.tx(t => {
                return t.batch([

                    t.oneOrNone(
                        req.sql.sync.batches_max_start_time,
                        { station_id: station_id }
                    ),

                    t.oneOrNone(
                        req.sql.sync.events_max_datetime,
                        { station_id: station_id }
                    ),
                ])
            })
            .then(data => {
                var batch_dt_from = data[0].start_time || '2017-06-01'
                var event_dt_from = data[1].datetime   || '2017-06-01'

                var batch_url = `http://${ip}:3000/api/data?type=batch&date_from=${batch_dt_from}`
                http.get( batch_url, response => {

                    if (response.statusCode !== 200) {
                        log.error(`get request ${batch_url} failed`);
                        return;
                    }

                    var rawData = '';
                    response.on('data', (chunk) => { rawData += chunk; });
                    response.on('end', () => {
                        try {
                            const parsedData = JSON.parse(rawData);
                            if (parsedData.length) {
                                db.none(`DELETE FROM batches WHERE station_id = $1 AND end_time IS NULL`, [ station_id ])
                                .then(() => {
                                    parsedData.forEach(datum => { datum.station_id = station_id })

                                    try {
                                        var values = new Inserts('${title}, ${start_time}, ${end_time}, ${start_count}, ${current_count}, ${status}, ${station_id}, ${local_id}', parsedData)
                                        db.none('INSERT INTO batches(title, start_time, end_time, start_count, current_count, status, station_id, local_id) VALUES $1', values)
                                        .then(data => {
                                            //log.info('success batches sync')
                                        })
                                        .catch(error => {
                                            log.error(`route ${req.originalUrl}, db query error: ${error}`)
                                        });
                                    }
                                    catch(e) {
                                        log.error(`get request ${batch_url} processing failed: ${e.message}`);
                                    }
                                })
                                .catch(error=> {
                                    log.error(`route ${req.originalUrl}, db query error: ${error}`)
                                });

                            }
                        } catch (e) {
                            log.error(`get request ${batch_url} processing failed: ${e.message}`);
                        }
                    });
                }).on('error', (e) => {
                    log.error(`get request ${batch_url} failed: ${e.message}`);
                });

                // получение данных о событиях
                var events_url = `http://${ip}:3000/api/data?type=event&date_from=${event_dt_from}`
                http.get( events_url, response => {

                    if (response.statusCode !== 200) {
                        log.error(`get request ${events_url} failed`);
                        return;
                    }

                    var rawData = '';
                    response.on('data', (chunk) => { rawData += chunk; });
                    response.on('end', () => {
                        try {
                            const parsedData = JSON.parse(rawData);
                            if (parsedData.length) {

                                parsedData.forEach(datum => { datum.station_id = station_id })

                                try {
                                    var values = new Inserts('${type_id}, ${value_int}, ${value_text}, ${datetime}, ${status}, ${station_id}, ${local_id}', parsedData)
                                    db.none('INSERT INTO events(type_id, value_int, value_text, datetime, status, station_id, local_id) VALUES $1', values)
                                    .then(data => {
                                        //log.info('success events sync')
                                    })
                                    .catch(error => {
                                        log.error(`route ${req.originalUrl}, db query error: ${error}`)
                                    });
                                }
                                catch(e) {
                                    log.error(`get request ${events_url} processing failed: ${e.message}`);
                                }
                            }
                        } catch (e) {
                            log.error(`get request ${events_url} processing failed: ${e.message}`);
                        }
                    });
                }).on('error', (e) => {
                    log.error(`get request ${events_url} failed: ${e.message}`);
                });

            })
            .catch(error => {
                log.error(`route ${req.originalUrl}, db query error: ${error}`)
            })
        }
    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })

    res.json({ success: 1 })
})

function Inserts(template, data) {
    if (!(this instanceof Inserts)) {
        return new Inserts(template, data);
    }
    this._rawDBType = true;
    this.formatDBType = function () {
        return data.map(d=>'(' + pgp.as.format(template, d) + ')').join();
    };
}

module.exports = router