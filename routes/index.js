var express = require('express')
var router  = express.Router()

var log     = require('../log.js')


router.get('/', function(req, res, next) {

    res.render('index', { title: 'Система управления станциями' })

})


router.get('/statistics', function(req, res, next) {

    req.db.oneOrNone(`
        SELECT * FROM stations WHERE id = $1
    `, [ req.query.station_id ])
    .then((station) => {
        var data = []
        var station_title = station.title
        var station_id = station.id 

        req.db.any(`
            SELECT
                id,
                title,
                start_time,
                end_time,
                to_char(start_time, 'DD.MM.YYYY') AS start_date,
                to_char(end_time, 'DD.MM.YYYY') AS end_date
            FROM
                batches
            WHERE
                station_id = $1
            ORDER BY id DESC
        `, [ station_id ])
        .then(function(batches) {
            var batch_id = req.query.batch_id
            if (batches.length) {
                batch_id = batch_id ||  batches[0].id
            }
            var min_date, max_date
            batches.forEach(function(batch){
                if (batch.id == batch_id) {
                    min_date = batch.start_time
                    max_date = batch.end_time
                }
            })
            var query        = 'station_id = $1 AND status = $2 AND events.type_id IN ($3, $4, $5) '
            var query_params = [station_id, 1, 10, 11, 13]
            if (min_date) {
                query += ' AND events.datetime > $6'
                query_params.push(min_date)
            }
            if (max_date) {
                query += ' AND events.datetime < $7'
                query_params.push(max_date)
            }
            req.db.any(`
            SELECT
                to_char(datetime, 'DD.MM.YYYY') AS date,
                to_char(datetime, 'YYYY-MM-DD') AS iso_date,
                MIN(CASE WHEN events.type_id != 11 THEN events.value_int ELSE 1000 END) AS min_weight,
                MAX(CASE WHEN events.type_id != 11 THEN events.value_int ELSE 0 END) AS max_weight,
                COUNT(CASE WHEN events.type_id != 11 THEN 1 END) AS total_weighings_count,
                COUNT(CASE WHEN events.type_id = 13 THEN 1 END) AS success_weighings_count,
                COUNT(CASE WHEN events.type_id = 11 THEN 1 END) AS death_count,
                SUM(CASE WHEN events.type_id != 11 THEN events.value_int ELSE 0 END) AS total
            FROM
                events
            WHERE ${query}
            GROUP BY
                date, iso_date
            ORDER BY
                iso_date DESC
            `, query_params)
            .then(data => {
                res.render('statistics', {
                    station_id:        station_id,
                    station_title:     station_title,
                    data:              data,
                    batches:           batches,
                    selected_batch_id: batch_id,
                    title:             `Станция - ${station_title} - статистика`
                })
            })
            .catch(error => {
                log.error(`route ${req.originalUrl}, db query error: ${error}`)
            })
        })
        .catch(error => {
            log.error(`route ${req.originalUrl}, db query error: ${error}`)
        })
    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })
 
})

router.get('/errors', function(req, res, next) {

    req.db.oneOrNone(`
        SELECT * FROM stations WHERE id = $1
    `, [ req.query.station_id ])
    .then(station => {
        var station_title = station.title
        var station_id = station.id
        req.db.any(`
    SELECT
        events.id AS id,
        events.value_int,
        to_char(datetime, 'HH24:MI:SS') AS time,
        to_char(datetime, 'DD.MM.YYYY') AS date
    FROM
        events
    WHERE events.status = 1
        AND events.type_id = 1
        AND events.station_id = $1
    ORDER BY
        events.datetime DESC
        `, [ station_id ]
        )
        .then(data => {
            res.render('errors', {
                station_title: station_title,
                title:         'Станция ' + station_title + ' - ошибки',
                data:          data,
            })
        })
        .catch(error => {
            log.error(`route ${req.originalUrl}, db query error: ${error}`)
        })
    })
    .catch(error => {
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
    })

    
})

module.exports = router
