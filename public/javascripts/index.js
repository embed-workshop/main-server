var station_states = [
    "остановлена (авария)",
    "обучение (открытый вход)",
    "взвешивание без отбора",
    "взвешивание с отбором",
    "запрос настроек",
    "обучение (все открыто)",
]

$(document).ready(function(){
    $.getJSON(
        '/api/stations/',
        function(data) {
            $('#tree').tree({
                data:        data,
                dragAndDrop: true,
                autoOpen:    true,

                onCanMove: function(node) {
                    if ( node.cpuid ) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },

                onCanSelectNode: function(node) {
                    if ( node.cpuid ) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },

                onCanMoveTo: function(moved_node, target_node, position) {
                    if (target_node.cpuid) {
                        return false;
                    }
                    else {
                        return (position == 'inside');
                    }
                },

                onCreateLi: function(node, $li) {
                    if ( node.cpuid ) {
                        $li.find('.jqtree-element').append(
                              '<span class="station_state circle" data-id="'+node.id +'"></span>'
                            + '<span class="station_mode">' + station_states[node.state] + '</span>'
                            + '<span class="station_cpuid">CPUID: ' + node.cpuid + '</span>'
                            + '<span class="station_ip">IP:    ' + node.ip + '</span>'
                            + '<a href="/statistics?station_id='+ node.id +'" class="station_stats" data-id="'+node.id +'">Статистика</a>'
                            + '<a href="/errors?station_id='+ node.id +'" class="station_breaks" data-id="'+node.id +'">Аварии</a>'
                            + '<a href="http://'+node.ip+':3000/panel" class="station_panel" target="_blank" data-id="'+node.id +'">Открыть панель управления</a>'
                        );
                    }
                }

            });

            $('#tree').bind(
                'tree.move',
                function(event) {
                    var station_id = event.move_info.moved_node.id
                    var station_group_id = event.move_info.target_node.id
                    $.post( `/api/stations/${station_id}/move_to_group/${station_group_id}`, {} )
                    .done(function( data ) {
                        if (data.success) {
                            console.log('ok')
                        }
                    })
                    .fail(function() {
                        alert('Не удалось добавить группу')
                    })
                    // console.log('moved_node', event.move_info.moved_node);
                    // console.log('target_node', event.move_info.target_node);
                    // console.log('position', event.move_info.position);
                    // console.log('previous_parent', event.move_info.previous_parent);
                }
            );

            $('#tree').bind(
                'tree.dblclick',
                function(event) {
                    // event.node is the clicked node
                    console.log(event.node);
                    rename_popup_toggle(1, event.node.id )
                }
            );



            poll()
        }
    );
})

function poll(){
    $.getJSON('/api/stations/state', function(data) {
        if (data.success) {
            $('.station_state').each(function(){
                var state = data.result[$(this).data('id')]
                if (typeof state !== 'undefined') {
                    $(this).removeClass('offline')
                    $(this).addClass('online')
                    $(this).siblings('.station_mode').text(station_states[state])
                }
                else {
                    $(this).removeClass('online')
                    $(this).addClass('offline')
                    $(this).siblings('.station_mode').text('--')
                }
            })
        }
        else {
            alert('server polling error')
        }
        setTimeout(poll, 60*1000)
    })
}

$(document).on('click', '.add_station_group', function(e){
    e.preventDefault()
    station_group_toggle(1)
})

$(document).on('click', '.cancel_station_group', function(e){
    e.preventDefault()
    station_group_toggle(0)
})

$(document).on('click', '.save_station_group', function(e){
    e.preventDefault()

    var title = $('.station_group_title').val()
    if (!title) {
        alert('Введите название группы!')
        return;
    }

    $.post('/api/stations/add', {title: title})
    .done(function( data ) {
        if (data.success) {
            $('#tree').tree(
                'appendNode',
                {
                    name: data.title,
                    id: 456
                }
            );
            station_group_toggle(0)
        }
    })
    .fail(function() {
        alert('Не удалось добавить группу')
    })
})

$(document).on('click', '#submit_rename', function(e){
    var new_title = $('#inpRename').val()
    if (!new_title) {
        alert('Введите новое название станции!');
        return;
    }

    var station_id = $('#rename_station_id').val()
    $.post(`/api/stations/${station_id}/rename`, { title: new_title })
    .done(function( data ) {
        if (data.success) {
            var node = $('#tree').tree('getNodeById', data.data.id);
            $('#tree').tree('updateNode', node, data.data.title);
            rename_popup_toggle(0)
        }
    })
    .fail(function() {
        alert('Не удалось переименовать станцию')
    })
})


$(document).on('click', '#cancel_rename', function(e){ 
    rename_popup_toggle(0)
})



$(document).keyup(function(e) {
    if (e.keyCode === 27) {
        station_group_toggle(0)
        rename_popup_toggle(0)
    }
})

function rename_popup_toggle(on, id) {
    if (on) {
        $('#rename_popup_modal').show()
        $('#rename_popup').show()
        $('#rename_station_id').val(id)
    }
    else {
        $('#rename_popup_modal').hide()
        $('#rename_popup').hide()
    }
}

function station_group_toggle(on) {
    if (on) {
        $('.add_station_group').hide()
        $('.save_station_group').show()
        $('.cancel_station_group').show()
        $('.station_group_title').show()
    }
    else {
        $('.add_station_group').show()
        $('.save_station_group').hide()
        $('.cancel_station_group').hide()
        $('.station_group_title').hide()
    }
}