function show_popup(text) {
    $('#GlobalMessage').html(
        `<div class="MessageType1"><p>${text}</p></div>`
    )

    setTimeout(function(){
        $('.MessageType1').fadeOut(1000)
    }, 5000)
}

