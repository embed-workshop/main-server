UPDATE
    stations
SET
    datetime = NOW(),
    state    = ${state}
WHERE
    id = ${id}