SELECT
    id, state
FROM
    stations
WHERE
    datetime > now() - interval '90 second'