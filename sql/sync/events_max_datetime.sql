SELECT
    to_char(MAX(datetime), 'YYYY-MM-DD HH24:MI:SS.US') AS datetime
FROM
    events
WHERE
    station_id = ${station_id}