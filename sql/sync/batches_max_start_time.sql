SELECT
   to_char(MAX(start_time), 'YYYY-MM-DD HH24:MI:SS.US') AS start_time
FROM
    batches
WHERE
    station_id = ${station_id}